import h5py
import numpy as np
from joblib import Parallel, delayed

from .constants import NUM_IMAGES_PER_INDIVIDUAL


def extract_images_and_labels_from_list(identification_images_file_paths):
    info = {}
    all_images = []
    all_labels = []
    for id, identification_images_file_path in enumerate(
        identification_images_file_paths
    ):
        print("Extracting images from {}".format(identification_images_file_path))
        images, labels, inf = extract_images_and_labels(identification_images_file_path)
        info[str(id)] = inf
        labels = [id] * len(images)
        all_images.extend(images)
        all_labels.extend(labels)
        print("{} images and {} labels".format(len(images), len(labels)))

    return all_images, all_labels, info


def extract_images_and_labels(identification_images_file_path):

    with h5py.File(identification_images_file_path, "r") as f:
        if "identities" in f.keys():
            if f["identification_images"].shape[0] != 0:
                # cnt = Counter([int(lb) for lb in f['identities'] if ~np.isnan(lb)])
                good_images = np.where(~np.isnan(f["identities"][:]))[0]
                if len(good_images) != 0:
                    images = f["identification_images"][good_images, ...]
                    labels = f["identities"][good_images, ...]
                else:
                    images = []
                    labels = []

            elif f["identification_images"].shape[0] == 0:
                images = []
                labels = []
        elif f["identification_images"].shape[0] != 0:
            p = np.clip(
                NUM_IMAGES_PER_INDIVIDUAL / f["identification_images"].shape[0],
                0.0,
                1.0,
            )
            images = [im for im in f["identification_images"] if np.random.rand() < p]
            labels = []

        else:
            images = []
            labels = []

        return images, labels


def extact_all_images_and_labels(identification_images_file_paths):

    Output = Parallel(n_jobs=-3)(
        delayed(extract_images_and_labels)(identification_images_file_path)
        for identification_images_file_path in identification_images_file_paths
    )

    images, labels = zip(*Output)

    images = [im for im in images if len(im) != 0]
    labels = [lb for lb in labels if len(lb) != 0]

    print(len(images), len(labels))

    return np.concatenate(images, axis=0), np.squeeze(np.concatenate(labels, axis=0))


def get_images_for_identity(images, labels, identity):
    identity_images = images[labels == identity]
    return identity_images
