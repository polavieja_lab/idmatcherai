"""
idmatcher.ai
=====
idmatcher.a provides a toolbox to match identities between videos tracked with
`idtracker.ai <http://idtracker.ai/>`_
-----
"""

import glob
import os
from pprint import pprint

import numpy as np
from natsort import natsorted

from .images import extact_all_images_and_labels, extract_images_and_labels_from_list
from .matcher import get_transfer_dicts, joined_results, match

# from network import train_model_from
from .summaries import summarize_matching  # , print_summary_matching

"""
# TODO: Think better the structure of folders and where to save.
Maybe VideoFolders instead of session folders.
Maybe better a list with two session folders or a list of lists of session folders
Think what escalates better 
# TODO: Think what happens when num animals is different
# TODO: Comment
"""


class IdMatcherAi(object):
    """Class to match identities between idtracker.ai sessions and between folders
    containing several idtracker.ai session for single animal videos.
    """

    def __init__(self, folderA, folderB, plot_results_flag=False, rematch_ids=False):
        """Initializes the class

        Parameters
        ----------
        folderA : str
            Path to the session folder of a video tracked with idtracker.ai or
            path to a folder containing multiple session folders from single
            animal videos. In the second case, the folder name cannot
            contain the name 'session'
        folderB :  str
            same as folderA
        save_folder : str
            Path to the folder where to save the results of the matching
        plot_results : bool
            If True plots a summary of the matching
        """
        self.rematch_ids = rematch_ids
        self.folderA = folderA
        self.folderB = folderB

        self.get_session_names()
        self.get_video_names()

        self.plot_results_flag = plot_results_flag

    def get_session_names(self):
        if self.two_tracked_videos():
            self.session_name_A = self.folderA.split("/")[-1]
            self.session_name_B = self.folderB.split("/")[-1]

    def get_video_names(self):
        if self.two_tracked_videos():
            video_name = (
                np.load(
                    os.path.join(self.folderA, "video_object.npy"), allow_pickle=True
                )
                .item()
                .video_path.split("/")[-1]
            )
            self.video_name_A = os.path.splitext(video_name)[0]
            video_name = (
                np.load(
                    os.path.join(self.folderB, "video_object.npy"), allow_pickle=True
                )
                .item()
                .video_path.split("/")[-1]
            )
            self.video_name_B = os.path.splitext(video_name)[0]

    def perform_matching(self):
        if self.two_tracked_videos():
            matching_results_A_folder = os.path.join(self.folderA, "matching_results")
            if not os.path.exists(matching_results_A_folder):
                os.makedirs(matching_results_A_folder)
            self.matching_results_file_A = os.path.join(
                matching_results_A_folder, self.video_name_B + "-.npy"
            )

            matching_results_B_folder = os.path.join(self.folderB, "matching_results")
            if not os.path.exists(matching_results_B_folder):
                os.makedirs(matching_results_B_folder)
            self.matching_results_file_B = os.path.join(
                matching_results_B_folder, self.video_name_A + "-.npy"
            )

        return (
            not os.path.isfile(self.matching_results_file_A)
            or not os.path.isfile(self.matching_results_file_B)
            or self.rematch_ids
        )

    def match_identities(self):
        """matches the identities between the two folders"""
        # two tracked videos

        if self.two_tracked_videos():
            if self.perform_matching():
                print("Matching from two tracked videos")
                self.get_models_folders()
                self.get_images_and_labels()
                self.get_matching_results()
                self.save_results()
            else:
                print("Matching found at {}".format(self.matching_results_file_A))
                print("Matching found at {}".format(self.matching_results_file_B))

        # single_animals images and video
        elif self.single_animal_videos():
            print("Matching from two sets of single animal videos")
            raise NotImplementedError
            # self.get_models_folders()
            # self.get_images_and_labels()
            # self.get_matching_results()

        else:

            raise ValueError("No matching possible")

    def save_results(self):
        np.save(self.matching_results_file_A, self.matching_results)
        np.save(self.matching_results_file_B, self.matching_results)

    def two_tracked_videos(self):
        """Returns true if folderA and folderB are session folders"""
        return "session" in self.folderA and "session" in self.folderB

    def single_animal_videos(self):
        """Returns True if folderA and folderB are not session folders.
        Note if folderA or folderB contain multiple session folders their name
        cannot contain the name 'session'
        """
        return "session" not in self.folderA and "session" not in self.folderB

    """Get models folders"""

    def get_models_folders(self):
        """Gets the path to the folder where the information about the CNN
        model is for both folders folderA and folderB.
        """
        print("Getting models ...")
        self.modelA = self.get_model_folders(self.folderA)
        self.modelB = self.get_model_folders(self.folderB)

    def get_model_folders(self, folder):
        """Returns the model folder given the main folder. If the main folder
        does not contain a valid model folder it will try to train a model
        with the information contained in the `folder`.
        """
        if self.has_model(folder):
            return self.get_model_folder_from(folder)
        else:
            raise RuntimeError("No model found in {}".format(folder))
            # return self.train_model(folder)

    def has_model(self, folder):
        """Checks if the folder contains any a folder with the 'accumulation'
        word. This indicates that `folder` is a session folder from idtracker.ai
        """
        return any(
            [
                ("accumulation" in path) or ("model" in path)
                for path in glob.glob(os.path.join(folder, "*"))
            ]
        )

    def get_model_folder_from(self, folder):
        """Finds the appropiate model folder from the `folder`. It basically
        selects the best model if the folder contains multiple models. This can
        happen if idtracker.ai entered in protocol 3 when tracking the video.
        """
        models_paths = [
            path
            for path in glob.glob(os.path.join(folder, "*"))
            if ("accumulation" in path) or ("model" in path)
        ]
        if len(models_paths) == 1:
            print("Model found in {}".format(models_paths[0]))
            return models_paths[0]
        else:
            print("Multiple models found in {}".format(folder))
            video_object_path = [
                path for path in glob.glob(os.path.join(folder, "*")) if "video" in path
            ]
            if len(video_object_path) == 1:
                print("Extracting best model from video_object")
                video_object = np.load(video_object_path[0], allow_pickle=True).item()
                model_name = video_object.accumulation_folder.split("/")[-1]
                return [
                    model_path
                    for model_path in models_paths
                    if model_name in model_path
                ][0]
            else:
                raise ValueError(
                    "Multiple models folders but video_object"
                    " not found. Cannot select right model"
                )

    # def train_model(self, folder):
    #     """Train model"""
    #     print("Training model for {}".format(folder))
    #     identification_images_paths = [
    #         p
    #         for p in glob.glob(os.path.join(folder, "*"))
    #         if "identification_images" in p
    #     ]
    #     sessions_paths = [
    #         p for p in glob.glob(os.path.join(folder, "*")) if "session" in p
    #     ]
    #     if len(identification_images_paths) == 1:
    #         identification_images_file_path = (
    #             identification_images_file_path
    #         ) = os.path.join(identification_images_paths[0], "i_images.hdf5")
    #         if os.path.isfile(identification_images_file_path):
    #             return train_model_from(identification_images_file_path)
    #         else:
    #             raise ValueError(
    #                 "No file found in {}".format(identification_images_paths[0])
    #             )
    #     elif len(sessions_paths) > 1:
    #         print("Multiple session folders found in {}".format(folder))
    #         identification_images_file_paths = [
    #             os.path.join(s, "identification_images", "i_images.hdf5")
    #             for s in sessions_paths
    #         ]
    #         if all([os.path.isfile(p) for p in identification_images_file_paths]):
    #             return train_model_from(identification_images_file_paths)
    #         else:
    #             raise ValueError(
    #                 "Some session folder in {} does not contain a i_imagees.hdf5 files".format(
    #                     folder
    #                 )
    #             )

    """ Get images nad labels """

    def get_images_and_labels(self):
        print("Getting images for {}".format(self.folderA))
        self.imagesA, self.labelsA = self.get_images_and_labels_from(self.folderA)
        self.imagesB, self.labelsB = self.get_images_and_labels_from(self.folderB)

    def get_images_and_labels_from(self, folder):
        identification_images_paths = [
            p
            for p in glob.glob(os.path.join(folder, "*"))
            if "identification_images" in p
        ]
        sessions_paths = glob.glob(os.path.join(folder, "session*"))
        if any(identification_images_paths) and len(identification_images_paths) == 1:
            print("identification_images folder found in {}".format(folder))
            identification_images_file_paths = natsorted(
                glob.glob(
                    os.path.join(identification_images_paths[0], "id_images_*.hdf5")
                )
            )
            return extact_all_images_and_labels(identification_images_file_paths)
        elif any(sessions_paths):
            identification_images_file_paths = natsorted(
                [
                    os.path.join(s, "identification_images", "id_images_*.hdf5")
                    for s in sessions_paths
                ]
            )
            return extract_images_and_labels_from_list(identification_images_file_paths)

    """ Matching """

    def get_matching_results(self):
        # match model A to images and labels in B
        print(
            "matching model {} to {} images and {} labels from {}".format(
                self.modelA, len(self.imagesB), len(self.labelsB), self.folderB
            )
        )
        confusion_matrix, frequencies_matrix, _ = match(
            self.modelA, self.imagesB, self.labelsB
        )
        transfer_dicts = get_transfer_dicts(confusion_matrix, frequencies_matrix)
        self.matching_results_A_B = summarize_matching(
            self.folderA,
            self.folderB,
            confusion_matrix,
            frequencies_matrix,
            transfer_dicts,
        )
        # match model B to images and labels in A
        print(
            "matching model {} to {} images and {} labels from {}".format(
                self.modelB, len(self.imagesA), len(self.labelsA), self.folderA
            )
        )
        confusion_matrix, frequencies_matrix, _ = match(
            self.modelB, self.imagesA, self.labelsA
        )
        transfer_dicts = get_transfer_dicts(confusion_matrix, frequencies_matrix)
        self.matching_results_B_A = summarize_matching(
            self.folderB,
            self.folderA,
            confusion_matrix,
            frequencies_matrix,
            transfer_dicts,
        )

        # joined resunts
        results = joined_results(self.matching_results_A_B, self.matching_results_B_A)
        self.matching_results = {
            "matching_results_A_B": self.matching_results_A_B,
            "matching_results_B_A": self.matching_results_B_A,
            "matching_results": results,
        }

        pprint(results["matches_dict_separated"]["hungarian_freq"])
        pprint(results["matches_dict_joined"]["hungarian_freq"])


def main():
    from argparse import ArgumentParser

    parser = ArgumentParser()
    parser.add_argument(
        "sessionA",
        help="path to the session folder with the results from the first video",
        type=str,
        default=None,
    )
    parser.add_argument(
        "sessionB",
        help="path to the session folder with the results from the second video",
        type=str,
        default=None,
    )
    parser.add_argument(
        "--save_folder",
        "-sf",
        help="folder where to save the results",
        type=str,
        default="./",
    )
    args = parser.parse_args()

    matcher = IdMatcherAi(args.sessionA, args.sessionB, args.save_folder)

    matcher.match_identities()


if __name__ == "__main__":
    main()
