import os


def summarize_matching(
    folder_model, folder_images, confusion_matrix, frequencies_matrix, transfer_dicts
):
    print_summary_matching(
        transfer_dicts,
        os.path.split(folder_images)[-2],
        os.path.split(folder_model)[-2],
    )
    matching_results = {
        "network_from": folder_model,
        "images_from": folder_images,
        "P1_confusion_matrix": confusion_matrix,
        "frequencies_matrix": frequencies_matrix,
        "transfer_dicts": transfer_dicts,
    }
    return matching_results


def print_summary_matching(transfer_dicts, fragments_session, network_session):

    assignment_types = list(transfer_dicts.keys())
    header = "  -  ".join(assignment_types)

    print("{} -> {} : ".format(fragments_session, network_session) + header)

    ids = transfer_dicts["max_freq"]["assignments"].keys()
    for i in ids:
        assignments = [
            str(
                (
                    transfer_dicts[assignment_type]["assignments"][i],
                    transfer_dicts[assignment_type]["assignments_values"][i],
                )
            )
            for assignment_type in assignment_types
        ]
        row_assignments = "  -  ".join(assignments)
        print("{} -> ".format(i) + row_assignments)
