# idmatcher.ai

⚠️ This package is deprecated! ⚠️

It's now included on [idtracker.ai](https://gitlab.com/polavieja_lab/idtrackerai/) since version 5.1.0. Check the [idtracker.ai webpage](https://idtracker.ai/) for more information about the main software.

Code related to SocialNet and [A study of transfer of information in animal collectives using deep learning tools](https://doi.org/10.1098/rstb.2022.0073) can be found in [fishandra repository](https://gitlab.com/polavieja_lab/fishandra/).
